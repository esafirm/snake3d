﻿using UnityEngine;
using System.Collections;

public class TutsSize : MonoBehaviour {
	new public Camera camera;
	// Use this for initialization
	void Start () {
		int tutsShown = PlayerPrefs.GetInt ("TutsShown", 0);
		//print ("tuts: " + tutsShown);
		#if !UNITY_WEBGL
		if (tutsShown > 2) {
		#endif
			gameObject.SetActive(false);
		#if !UNITY_WEBGL
			return;
		}
		#endif
		tutsShown++;
		PlayerPrefs.SetInt ("TutsShown", tutsShown);


		RectTransform rectTra = GetComponent<RectTransform> ();
		float height = camera.orthographicSize * 2;
		float width = camera.aspect * height;
		rectTra.sizeDelta = new Vector2 (width, height);
	}
	

}
