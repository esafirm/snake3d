﻿using UnityEngine;
using System.Collections;

public class RotateAnimation : MonoBehaviour {
	public float freq;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate (new Vector3 (0f, 360 * freq * Time.deltaTime, 0f));
	}
}
