﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;

public class MenuItem : MonoBehaviour {
	// Use this for initialization
	void Start () {
		onChoosed.AddListener(new UnityEngine.Events.UnityAction(PrintName));
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown (KeyCode.Space)) {
			onChoosed.Invoke ();
		}
	}

	public UnityEngine.Events.UnityEvent onChoosed;

	public void PrintName(){
		//print (name);
	}

	public void Choosed(){
		Invoke ("InvokeOnChoosed", 0.5f);
	}

	private void InvokeOnChoosed(){
		onChoosed.Invoke();
	}
}
