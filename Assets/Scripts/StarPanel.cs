﻿using UnityEngine;
using System.Collections;

public class StarPanel : MonoBehaviour {
	public GameObject star;
	Animator[] starsAnim;
	int lastShown = -1;
	bool inited = false;
	void TryInit(){
		if (!inited) {
			float halfWidth = GetComponent<RectTransform> ().rect.width / 2f;
			starsAnim = new Animator[3];
			for (int i = -1; i <= 1; i++) {
				float x = i * halfWidth;
				var starIns = Instantiate (star);
				starIns.transform.SetParent (transform);
				starIns.transform.localPosition = new Vector3 (x, 0f, 0f);
				starsAnim [i + 1] = starIns.GetComponent<Animator> ();
			}
			inited = true;
		}

	}

	void OnEnable(){
		TryInit ();
	}

	public void ShowStar(){
		TryInit ();	
		lastShown++;

		if (lastShown >= starsAnim.Length)
			return;
		starsAnim [lastShown].speed = 1f;
		starsAnim [lastShown].enabled = true;
		//print ("done");
	}
}
