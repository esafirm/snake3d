﻿using UnityEngine;
using System.Collections.Generic;

[ExecuteInEditMode]
public class SnakeProcedural : MonoBehaviour {
	public int circleN = 16;
	int lengthN = 16;
	public int lengthNPerLength = 15;
	public int lengthNPerHead = 15;
	public float length = 1f;

	public float deltaT = 0.2f;

	public float radius = 0.5f;
	public bool doMiddle = true;
	public Vector3 middleIndent = new Vector3 (0f, 0.5f, 0f);
	//public int temp;
	// Use this for initialization
	void Start () {
		trianglesNotHead = new List<int> ();
		trianglesHead = new List<int> ();
		verts = new List<Vector3> (); 
		uvs = new List<Vector2> ();  
	}
	List<int> trianglesHead;
	List<int> trianglesNotHead;
	List<Vector3> verts;
	List<Vector2> uvs;
	// Update is called once per frame
	void Update () {
		((SupremeCurve)middleCurve).UpdateCurvesList();

		lengthN = Mathf.CeilToInt(Mathf.Clamp(length-1,0, 10000f) * lengthNPerLength + Mathf.Clamp01(length) * lengthNPerHead);
		MeshFilter mf = GetComponent<MeshFilter> ();
		if (mf.sharedMesh == null) {
			mf.sharedMesh = new Mesh ();
		}
		mf.sharedMesh.name = "Procedural snake";
		Mesh mesh = mf.sharedMesh;


		trianglesHead.Clear ();
		trianglesNotHead.Clear ();
		verts.Clear();
		uvs.Clear();
		float PI2 = Mathf.PI * 2;
		int k = 0; 
		//float y = 0.5f;
		bool isHeadLast = false;
		for (int j = 0; j < lengthN; j++) {
			//if (j >= temp)
			//	continue;
			float tz =0f;
			bool isHead = tz >= length - 1f;
			if(j < lengthN - lengthNPerHead){
				tz = (1f * (j) / (lengthNPerLength));
				isHead = false;
			}else{
				tz = length - 1f + (j - lengthN + lengthNPerHead) * 1f / (lengthNPerHead - 1);
				isHead = true;
				//tz = Mathf.Repeat (tz, 1f);
			}
			float z = tz - 0.5f;
			Vector3 middle;
			int middleK = -1;

			if(doMiddle)
			if (j == 0 || j == lengthN-1) {
				middle = Middle (tz);
				if (j == lengthN - 1)
					middleK = k + circleN;
				else{
					middleK = k++;
					verts.Add (middle);
					uvs.Add (new Vector2 (z, 0.5f));
				}
			}

			Vector3 extraScale = ExtraScale (tz);
			Quaternion rotationTz = Rotation (tz);
			Vector3 middleTz = Middle (tz);
			var triangles = isHead && isHeadLast ? trianglesHead : trianglesNotHead;
			isHeadLast = isHead;
			for (int i = 0; i < circleN; i++) {

				float t = (1f * i / (circleN - 1));
				float radius = Radius (tz);
				Vector3 fromCircle = new Vector3 (Mathf.Sin (t * PI2 + Mathf.PI) * radius, Mathf.Cos (t * PI2 + Mathf.PI) * radius, 0f);
				fromCircle.Scale (extraScale);
				Vector3 v = rotationTz * fromCircle + middleTz;

				verts.Add (v);

				uvs.Add (new Vector2 (tz, t));


				if (doMiddle && middleK != -1) {
					if(j != 0)
						triangles.Add (middleK);
					triangles.Add (k);

					if(j == 0)
						triangles.Add (middleK);
					triangles.Add (i == 0 ? k + circleN-1 : k-1);
				}
				if (j != 0) {

					triangles.Add (k);
					triangles.Add (k-circleN);
					triangles.Add (i == 0 ? k + circleN-1 : k-1);


					triangles.Add (k);
					triangles.Add (i == circleN - 1 ? k-circleN+1 - circleN : k-circleN+1);
					triangles.Add (k-circleN);
				}
				k++;
			}
			if (j == lengthN - 1) {
				middle = Middle (tz);
				verts.Add (middle);
				uvs.Add (new Vector2 (tz-0.000001f, 0.5f));
			}
		}
		mesh.Clear ();
		mesh.subMeshCount = 2;

		if (length > 0.001f) {
			//GetComponent<MeshRenderer> ().enabled  = true;
			mesh.vertices = verts.ToArray ();
			mesh.SetTriangles (trianglesNotHead, 0);
			mesh.SetTriangles (trianglesHead, 1);
			mesh.uv = uvs.ToArray ();
			mesh.RecalculateBounds ();
			mesh.RecalculateNormals ();
		} else {
			//GetComponent<MeshRenderer> ().enabled = false; 
		}
	}

	public Quaternion Rotation(float t){
		return middleCurve.GetRotAt(t+curveT+deltaT);
	}

	public Vector3 Middle(float t){
		return middleCurve.GetPosAt(t+curveT+deltaT) + middleIndent;
	}

	public float Radius(float t){
		return radius;
	}

	public float curveT = 0f;

	public Curve middleCurve;


	public Vector3 ExtraScale(float t){
		if (t >= length - 1) {
			float y = headShapeY.Evaluate (t - length + 1);
			float x = headShapeX.Evaluate (t - length + 1);
			//print (t - length + 1);
			return new Vector3 (x, y, 1f);
		} else {
			if (t < 1) {
				return Vector3.one * tailShape.Evaluate (t);
			} else {
				return Vector3.one;
			}
		}

	}

	public AnimationCurve headShapeX;
	public AnimationCurve headShapeY;

	public AnimationCurve tailShape;
}
