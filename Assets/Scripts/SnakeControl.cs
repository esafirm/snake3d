﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class SnakeControl : MonoBehaviour {
	Field f;
	DirectionChooser dirChooser;
	SupremeCurve curve;
	[HideInInspector]
	public SnakeProcedural snakeProc;
	//[SerializeField]
	public bool move{get;set;}
	//public bool fasterAtFinish = false;

	// Use this for initialization
	void Start () {
		f = FindObjectOfType<Field> ();
		snakeProc = FindObjectOfType<SnakeProcedural> ();
		curve = snakeProc.middleCurve.GetComponent<SupremeCurve> ();
		dirChooser = snakeProc.middleCurve.GetComponent<DirectionChooser> ();
		ui = GameObject.Find ("UI");

			snakeProc.length = lengthInit;
			snakeProc.curveT = -lengthInit + curveTInit;

		Update ();

	}

	


	float lastMoveTime;
	float lastFallTime = -10f;
	//float timeBetweenFalls = 0.5f;
	public float timeBetweenMoves = 1f;
	public float minusTimePerApple = 0.01f;
	public float minimumTime = 0.2f;
	public int lengthInit;
	public float curveTInit = 1f;
	// Update is called once per frame

	public enum MoveDirection{
		NONE, LEFT, RIGHT, FORWARD, BACK
	}

	void Update () {
		if (!dead && !finish) {
			if (!IsGround (transform.position - Vector3.up) && !IsGround (transform.position - Vector3.up * 1.5f) && !IsGround (transform.position - Vector3.up * 0.5f)) {



				if (Time.time > lastFallTime + timeBetweenMoves / 2) { 
					lastFallTime = Time.time;
					lastMoveTime = lastFallTime;
					if (!faling) {
						dirChooser.CreateByDirection (HalfCircleCurve.Direction.bot);
						faling = true;
						//PlayMoveSound ();
					} else {
						dirChooser.CreateByDirection (HalfCircleCurve.Direction.forward);
						//PlayMoveSound ();
					}
				}
			} else {
				MoveDirection direction = GetMoveDirection ();
				if (direction != MoveDirection.NONE && direction != MoveDirection.BACK) {
					move = true;
				}
				if(move){
					if (direction == MoveDirection.LEFT) {
						dirChooser.CreateByDirection (HalfCircleCurve.Direction.left);
						lastMoveTime = Time.time;
						faling = false;
						PlayMoveSound ();
					}

					if (direction == MoveDirection.RIGHT) {
						dirChooser.CreateByDirection (HalfCircleCurve.Direction.right);
						lastMoveTime = Time.time;
						faling = false;
						PlayMoveSound ();
					}

					if (direction == MoveDirection.FORWARD) {
						MoveForward ();
						PlayMoveSound ();
					}

					if (direction == MoveDirection.BACK) {
						if (faling) {
							dirChooser.CreateByDirection (HalfCircleCurve.Direction.bot);
							faling = false;
							lastMoveTime = Time.time;
						}
					}

					if (Time.time > lastMoveTime + timeBetweenMoves) {
						MoveForward ();
						//ui.GetComponent<UIScript> ().PlayMove ();
					}
				}
			}
			transform.position = curve.transform.GetChild (curve.transform.childCount - 1).position;
			var apple = IsApple (transform.position);
			if (apple) {
				snakeProc.length += 1f;
				snakeProc.curveT -= 1f;
				apple.GetComponent<Food> ().UpdateSnake (this);
				ui.GetComponent<UIScript> ().PlayPick ();
				Destroy (apple);
				applesEaten++;
				timeBetweenMoves -= minusTimePerApple;
				timeBetweenMoves = Mathf.Max (minimumTime, timeBetweenMoves);
				f.foodCount--;

			}
			UpdateImaginaryParts ();
			if (IsGround (transform.position - Vector3.up * 0.5f)) {
				curve.transform.GetChild (curve.transform.childCount - 1).position += new Vector3 (0f, 0.5f, 0f);
				curve.transform.GetChild (curve.transform.childCount - 2).gameObject.GetComponent<HalfCircleCurve> ().halfUp = true;
				transform.position = curve.transform.GetChild (curve.transform.childCount - 1).position;
				//print ("hellllllooooooooo");
			} else {
				if (!IsGround (transform.position - Vector3.up) && IsGround (transform.position - Vector3.up * 1.5f)) {
					curve.transform.GetChild (curve.transform.childCount - 1).position -= new Vector3 (0f, 0.5f, 0f);
					curve.transform.GetChild (curve.transform.childCount - 1).gameObject.GetComponent<HalfCircleCurve> ().halfDown = true;
				}
			}

		


			transform.position = curve.transform.GetChild (curve.transform.childCount - 1).position;

			transform.eulerAngles = Vector3.Scale(curve.transform.GetChild (curve.transform.childCount - 1).eulerAngles, new Vector3(0,1,0)) ;

			CheckIfHalfUpNeeded ();
			CheckHeadIntersect ();
			if (move || menuMode) {
				UpdateImaginaryParts ();
				UpdateProcedural ();
			}
		}
		if (finish) {
			FinishShrink ();
			FinishShrinkImaginary ();
		}
		if (dead) {
			UpdateProcedural ();
		}
	}

	public AudioClip moveSound;

	public void PlayMoveSound(){
		GetComponent<AudioSource> ().PlayOneShot (moveSound);
	}

	public void CheckIfHalfUpNeeded(){
		if (IsGround (transform.position + curve.transform.GetChild (curve.transform.childCount - 1).forward - Vector3.up * 0.5f)) {
			curve.transform.GetChild (curve.transform.childCount - 1).gameObject.GetComponent<HalfCircleCurve> ().halfUp = true;
		} else {

		}
	}
	//public bool constantSpeed = false;
	public void UpdateProcedural(){
		float headT = snakeProc.curveT + snakeProc.length;

		float delta = curve.transform.childCount - headT; 
		if (menuMode) {
			delta -= 0.5f;
		}
		//print (delta);

		float speed = 1f;
		if (delta <= 0) {
			speed = 0f;
		} else {
			speed = 1f/timeBetweenMoves * (int)(delta > 0 ? (delta > 1f ? delta*2f : 1f) : 0f);
		}
		if (menuMode) {
			speed = Mathf.Clamp01(delta) * 1f / timeBetweenMoves;

		}


		snakeProc.curveT += Time.deltaTime * speed;
		if (!move && menuMode) {
			//snakeProc.curveT = Mathf.Clamp (snakeProc.curveT, -100f, 4.5f);
		}
	}

	bool controlLocked = false;
	public void LockControl(){
		controlLocked = true;
	}
	public bool menuMode = false;
	public void FinishShrink(){
		float speed = 1f;
		if (!menuMode) {
			speed = 2f / timeBetweenMoves;
		} else {
			speed = 0.5f / timeBetweenMoves;
		}

		snakeProc.curveT += 2f*Time.deltaTime * speed;
		//snakeProc.length -= Time.deltaTime * speed;

		snakeProc.length = Mathf.Clamp (snakeProc.length, 0, 999);
	}

	float finishImaginaryShrink;
	public void FinishShrinkImaginary(){
		float speed = 1f;
		if (!menuMode) {
			speed = 2f / timeBetweenMoves;
		} else {
			speed = 0.5f / timeBetweenMoves;
		}

		finishImaginaryShrink += 2f * Time.deltaTime * speed;

		while (0f < finishImaginaryShrink) {
			finishImaginaryShrink--;
			transform.GetChild (0).position = new Vector3(-10000, -100000, -100000);
			for (int i = transform.childCount-1; 0 < i; i--) {
				transform.GetChild (i).position = transform.GetChild (i - 1).position;						
			}
		}
		//snakeProc.length -= Time.deltaTime * speed;

		snakeProc.length = Mathf.Clamp (snakeProc.length, 0, 999);
	}


	public enum ForwardDirection{
		X, mX, Z, mZ
	}

	//public ForwardDirection FORWARD;
	//public MoveDirection move;

	bool rightBot = false;
	public void MoveRightBot(){
		rightBot = true;
	}

	bool rightTop = false;
	public void MoveRightTop(){
		rightTop = true;
	}

	bool leftBot = false;
	public void MoveLeftBot(){
		leftBot = true;
	}

	bool leftTop = false;
	public void MoveLeftTop(){
		leftTop = true;
	}

	bool rotateAntiClockwise = false;
	public void RotateAntiClockwise(){
		rotateAntiClockwise = true;
	}


	bool rotateClockwise = false;
	public void RotateClockwise(){
		rotateClockwise = true;
	}

	MoveDirection GetMoveDirection(){
		MoveDirection result = MoveDirection.NONE;
		ForwardDirection forward = ForwardDirection.Z;

		if (controlLocked) {
			return MoveDirection.NONE;
		}

		float euY = transform.eulerAngles.y;

		if (45f < euY && euY < 135f)
			forward = ForwardDirection.X;
		else if (135f < euY && euY < 225f)
			forward = ForwardDirection.mZ;
		else if (225f < euY && euY < 315f)
			forward = ForwardDirection.mX;
		else if (315f > euY || euY < 45f)
			forward = ForwardDirection.Z;

		//FORWARD = forward;
		KeyCode mZ = KeyCode.G;
		KeyCode Z = KeyCode.N;
		KeyCode X = KeyCode.B;
		KeyCode mX = KeyCode.H;


		if (Input.GetKeyDown (Z) || rightBot) {
			//print ("ZZZZZ");
			switch (forward) {
			case ForwardDirection.X:
				result = MoveDirection.LEFT;
				break;
			case ForwardDirection.mX:
				result = MoveDirection.RIGHT;
				break;
			case ForwardDirection.Z:
				result = MoveDirection.FORWARD;
				break;
			case ForwardDirection.mZ:
				result = MoveDirection.BACK;
				break;
			}
		}

		if (Input.GetKeyDown (mZ) || leftTop) {
			switch (forward) {
			case ForwardDirection.X:
				result = MoveDirection.RIGHT;
				break;
			case ForwardDirection.mX:
				result = MoveDirection.LEFT;
				break;
			case ForwardDirection.Z:
				result = MoveDirection.BACK;
				break;
			case ForwardDirection.mZ:
				result = MoveDirection.FORWARD;
				break;
			}
		}

		if (Input.GetKeyDown (X) || leftBot) {
			switch (forward) {
			case ForwardDirection.X:
				result = MoveDirection.FORWARD;
				break;
			case ForwardDirection.mX:
				result = MoveDirection.BACK;
				break;
			case ForwardDirection.Z:
				result = MoveDirection.RIGHT;
				break;
			case ForwardDirection.mZ:
				result = MoveDirection.LEFT;
				break;
			}
		}

		if (Input.GetKeyDown (mX) || rightTop) {
			switch (forward) {
			case ForwardDirection.X:
				result = MoveDirection.BACK;
				break;
			case ForwardDirection.mX:
				result = MoveDirection.FORWARD;
				break;
			case ForwardDirection.Z:
				result = MoveDirection.LEFT;
				break;
			case ForwardDirection.mZ:
				result = MoveDirection.RIGHT;
				break;
			}
		}

		if (Input.GetKeyDown (KeyCode.A) || rotateAntiClockwise) {
			result = MoveDirection.LEFT;
		}
		if (Input.GetKeyDown (KeyCode.D) || rotateClockwise) {
			result = MoveDirection.RIGHT;
		}


		rightBot = false;
		rightTop = false;
		leftBot = false;
		leftTop = false;
		rotateClockwise = false;
		rotateAntiClockwise = false;

		if (menuMode && result != MoveDirection.NONE && result != MoveDirection.BACK){
			LockControl ();
		}

		return result;
	}


	bool faling = false;

	public void CheckHeadIntersect(){
		List<GameObject> objs = f.GetAt (transform.position);
		objs.AddRange(f.GetAt(transform.position + Vector3.up * .5f));

		foreach (GameObject o in objs) {
			if (o.tag == "Wall" || o.tag == "Snake" && o != GetHead() || o.tag == "Trap") {				
				Die ();
			}

			if (o.tag == "Exit") {		
				if (menuMode) {
					finish = true;

					o.GetComponent<MenuItem> ().Choosed ();
				} else {
					Finish ();
				}
			}
		}
	}

	GameObject GetHead(){
		return transform.GetChild (0).gameObject;
	}

	public bool IsWalkable(Vector3 point){
		List<GameObject> objs = f.GetAt (point);

		foreach (GameObject o in objs) {
			if (o.tag == "Wall" || o.tag == "Snake")
				return false;
		}
		return true;
	}

	public bool IsGround(Vector3 point){
		List<GameObject> objs = f.GetAt (point);

		foreach (GameObject o in objs) {
			if (o.tag == "Wall" || o.tag == "Snake")
				return true;
		}
		return false;
	}

	public bool IsWall(Vector3 point){
		List<GameObject> objs = f.GetAt (point);

		foreach (GameObject o in objs) {
			if (o.tag == "Wall")
				return true;
		}
		return false;
	}

	public GameObject IsApple(Vector3 point){
		List<GameObject> objs = f.GetAt (point);
		objs.AddRange( f.GetAt (point+Vector3.up*0.5f));
		objs.AddRange( f.GetAt (point-Vector3.up*0.5f));

		foreach (GameObject o in objs) {
			if (o.tag == "Food")
				return o;
		}
		return null;
	}

	public void UpdateImaginaryParts(){
		int delta = transform.childCount - Mathf.RoundToInt (snakeProc.length);
		for(int i = 0; i < delta; i++){
			var o = transform.GetChild (delta-1-i);
			Destroy (o);
		}

		while (transform.childCount < Mathf.RoundToInt (snakeProc.length)) {
			var o = Instantiate (snakeImaginaryPart);
			o.transform.parent = transform;
			
		}
		for (int i = 0; i < transform.childCount; i++) {
			Transform child = transform.GetChild(i);
			child.gameObject.tag = "Snake";
			Vector3 pos = new Vector3 (-100000, 0, -100000);
			if(curve.transform.childCount - i > 0) 
				pos = curve.transform.GetChild (curve.transform.childCount - 1 - i).position;
			child.position = pos;
		}
	}
	public GameObject snakeImaginaryPart;

	public void MoveForward(){
		if (faling) {
			dirChooser.CreateByDirection (HalfCircleCurve.Direction.top);
			faling = false;
		}
		else
			dirChooser.CreateByDirection (HalfCircleCurve.Direction.forward);
		lastMoveTime = Time.time;
		faling = false;
	}


	public void TryFallAll(){
		int fallDist = 2;

		for (int i = 0; i < transform.childCount; i++) {
			int dist = 2;
			if(IsWall(curve.transform.GetChild(curve.transform.childCount-1-i).transform.position + Vector3.down *1.5f)){
				dist = 1;
			}
			if(IsWall(curve.transform.GetChild(curve.transform.childCount-1-i).transform.position + Vector3.down *1f)){
				dist = 0;
			}
			fallDist = Mathf.Min(dist, fallDist);
		}
		while (fallDist > 0) {
			fallDist--;
			for(int i = 0; i < transform.childCount; i++){
				curve.transform.GetChild(curve.transform.childCount-1-i).transform.position += Vector3.down * 0.5f;
			}
		}
	}

	bool dead;
	GameObject ui; 
	public void Die(){
		ui.GetComponent<UIScript> ().PlayLose ();
		//print ("die");
		dead = true;
		var losePanel = ui.transform.FindChild ("LosePanel").gameObject;
		//var andControl = ui.transform.FindChild ("AndroidControl").gameObject;
		//andControl.SetActive(false);
		//Application.LoadLevel (Application.loadedLevelName);
		if (SceneManager.GetActiveScene ().name.Contains ("snakeRand")) {
			ui.GetComponent<UIScript> ().UpdateApplesTotal ();
			losePanel.transform.FindChild ("Score").GetComponent<UnityEngine.UI.Text> ().text = applesEaten+"";
			//print ("labudabudabda");
		}
		losePanel.SetActive(true);

	}
	public int applesEaten = 0;
	bool finish = false;
	public void Finish(){
		ui.GetComponent<UIScript> ().PlayWin ();
		finish = true;
		//print ("win, Apples = " + applesEaten);
		timeBetweenMoves /= 2f;
		//dead = true;
		var winPanel = ui.transform.FindChild ("WinPanel").gameObject;
		var andControl = ui.transform.FindChild ("AndroidControl").gameObject;
		//andControl.SetActive(false);
		winPanel.SetActive(true);
		//Application.LoadLevel (Application.loadedLevelName);
		/*
		if (Application.loadedLevelName != "snakeRand") {
			PlayerPrefs.SetInt ("LastFinished", Application.loadedLevel);
			PlayerPrefs.SetInt ("Max finished level", Mathf.Max (PlayerPrefs.GetInt ("Max finished level"), Application.loadedLevel - 1));
		}
		*/

		if (!SceneManager.GetActiveScene().name.Contains("snakeRand")) {
			PlayerPrefs.SetInt ("LastFinished", SceneManager.GetActiveScene().buildIndex);
			PlayerPrefs.SetInt ("Max finished level", Mathf.Max (PlayerPrefs.GetInt ("Max finished level"), SceneManager.GetActiveScene().buildIndex));
		}

		ui.GetComponent<UIScript> ().UpdateApplesTotal ();
		//curve.transform.GetChild (curve.transform.childCount - 1).gameObject.GetComponent<HalfCircleCurve>();
	}
}
