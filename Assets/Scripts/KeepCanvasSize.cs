﻿using UnityEngine;
using System.Collections;
[ExecuteInEditMode]
public class KeepCanvasSize : MonoBehaviour {
	new public Camera camera;
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {

		var size = new Vector2 ();
		Camera c = camera == null ? Camera.main : camera;
		size = new Vector2(Camera.main.orthographicSize*2 * Camera.main.aspect, Camera.main.orthographicSize*2);
		//print (Camera.main.orthographicSize*2);
		GetComponent<RectTransform> ().sizeDelta = size;
	}
}
