﻿using UnityEngine;
using System.Collections.Generic;

public class SupremeCurve : Curve {
	List<Curve> curves = new List<Curve>();

	void Update(){
		lastT = transform.childCount;
		UpdateCurvesList ();
	}

	public void UpdateCurvesList(){
		while (curves.Count < transform.childCount) {
			curves.Add (transform.GetChild (curves.Count).GetComponent<Curve>());
		}
	}

	public override Vector3 GetPosAt(float t){
		int child = (int)t;

		child = Mathf.Clamp (child, 0, transform.childCount-1);

		return curves[child].GetPosAt(t-child);
	}

	public override Quaternion GetRotAt(float t){
		int child = (int)t;

		child = Mathf.Clamp (child, 0, transform.childCount-1);
		return curves[child].GetRotAt(t-child);
	}

	public void Set(SupremeCurve other){
		foreach (Transform t in transform) {
			Destroy (t.gameObject);
		}

		foreach (Transform t in other.transform) {
			Vector3 pos = t.position;
			Quaternion rotation = t.rotation;
			((GameObject)Instantiate (t.gameObject, pos, rotation)).transform.SetParent(transform);
		}
	}


}
