﻿using UnityEngine;
using System.Collections.Generic;
[ExecuteInEditMode]
public class RandomField : MonoBehaviour {
	public int width = 10;
	public int depth = 10;
	public int heigth = 10;
	
	public GameObject snake;
	public GameObject trap;
	public GameObject wall;
	public GameObject food;
	public GameObject foodCool;
	public float coolFoodPercent = 0.07f;

	// Use this for initialization
	public bool update = false;
	public bool updateOnce = false;
	public bool updateOnStart = false;
	public bool clear = false; 

	private Transform root;
	private Transform wallRoot;
	private Transform foodRoot;
	private Transform trapRoot;
	void Start () {
		field = GetComponent<Field>();
		if (updateOnStart)
			updateOnce = true;
		CreateRoot ();
		walkables = GameObject.Find ("Walkables");
		field.foodCount = CountFood ();
		Update ();
		/*
		if (Application.isPlaying) {
			updateOnce = true;
		}
		*/

	}
	Field field;


	void CreateRoot(){
		
		if (!transform.FindChild ("For Generated")) {
			root = new GameObject("For Generated").transform;
			root.parent = transform;
		}
		root = transform.FindChild ("For Generated");


		
		if (!root.transform.FindChild ("Walls")) {
			wallRoot = new GameObject("Walls").transform;
			wallRoot.transform.parent = root;
		}
		wallRoot = root.FindChild ("Walls");
		
		
		if (!root.transform.FindChild ("Traps")) {
			trapRoot = new GameObject("Traps").transform;
			trapRoot.transform.parent = root;
		}
		trapRoot = root.FindChild ("Traps");

		
		
		if (!root.transform.FindChild ("Foods")) {
			foodRoot = new GameObject("Foods").transform;
			foodRoot.transform.parent = root;
		}
		foodRoot = root.FindChild ("Foods");
	}
	public bool walls;
	public bool dontUpdate;
	public bool genFloor = true;
	// Update is called once per frame
	void Update () {
		if ((update || updateOnce) && !(dontUpdate)) {
			CreateRoot();
			Clear();
			CreateRoot();
			if(randomTraps)
				GenRandomTraps();
			if(genFloor)
				GenFloor();
			if(walls)
				GenRoundWall();
			if(trap != null)
				GenUnderTraps();
			if(randomWalls)
				GenRandomWalls();
			updateOnce = false;
		}
		if (genFood && Application.isPlaying) {
			int foodsCount = field.foodCount;
			for (int i = foodsCount; i < genIfLessThan; i++) {
				Vector3 pos = RandomEmptyPoint (0f);
				if (pos != nullVector)
					SingleFood (pos.x, pos.y, pos.z);
			}
		}

		if (clear) {
			Clear();
			clear = false;
		}
	}
	public bool genFood;
	public int genIfLessThan = 3;
	public int CountFood(){
		int sum = 0;
		foreach (Transform t in transform.GetComponentsInChildren<Transform>()) {
			if(t.CompareTag("Food"))
				sum++;
		}
		return sum;
	}

	public void GenUnderTraps(){
		for(int x = -1; x < width+1; x++){
			for(int z = -1; z < depth+1; z++){
				SingleTrap(x, -2f, z);
			}
		}
	}

	public void Clear(){
		foreach (Transform t in root.GetComponentsInChildren<Transform>()) {
			if(t == transform || t == root || t == wallRoot || t == foodRoot || t == trapRoot)
				continue;
			if(t!= null)
			if(Application.isPlaying){
				Destroy(t.gameObject);
			}else{
				DestroyImmediate(t.gameObject);
			}
		}
	}

	public void GenFloor(){
		if (!wall) {
			//print ("need wall");
			return;
		}
		for(int x = 0; x < width; x++){
			for(int z = 0; z < depth; z++){
				SingleWall(x, -1f, z);
			}
		}
	}
	GameObject walkables;
	public Vector3 RandomEmptyPoint(float height){
		var dict = field.GetObjectDictionary ();


		List<Vector3> points = new List<Vector3>();

		if (walkables != null) {
			foreach (Transform t in walkables.transform) {
				Vector3 p = new Vector3(Mathf.RoundToInt(t.position.x*2f)/2f, Mathf.RoundToInt(t.position.y*2f)/2f, Mathf.RoundToInt(t.position.z*2f)/2f);
				if (!dict.ContainsKey(p))
					points.Add (p);
			}		
		} else {
			for (int x = 0; x < width; x++) {
				for (int z = 0; z < depth; z++) {
					//Vector3 p = new Vector3 (x, height, z);
					Vector3 p = new Vector3(Mathf.RoundToInt(x*2f)/2f, Mathf.RoundToInt(height*2f)/2f, Mathf.RoundToInt(z*2f)/2f);

					//if (f.GetAt (p).Count == 0)
					if (!dict.ContainsKey(p))
						points.Add (p);
				}
			}
		}
		return points.Count > 0 ? points [Random.Range (0, points.Count - 1)] : nullVector;
	}
	public bool randomWalls = true;
	public int randWallCount = 5;
	public void GenRandomWalls(){
		for(int i = 0; i < randWallCount;i++){
			var pos = RandomEmptyPoint(0f);
			if(pos != nullVector){
				SingleWall(pos.x, pos.y, pos.z);
			}
		}
	}
	
	public bool randomTraps = true;
	public int randTrapCount = 5;
	public void GenRandomTraps(){
		for(int i = 0; i < randWallCount;i++){
			var pos = RandomEmptyPoint(0f);
			if(pos != nullVector){
				SingleTrap(pos.x, pos.y, pos.z);
			}
		}
	}
	private Vector3 nullVector = new Vector3(666,666,666);

	private void SingleWall(float x, float y, float z){
		GameObject newWall;
		#if UNITY_EDITOR 
		newWall = (GameObject)UnityEditor.PrefabUtility.InstantiatePrefab (wall);
		newWall.transform.position = new Vector3(x, y, z);
		#else

		newWall = (GameObject)Instantiate(wall, new Vector3(x, y, z), wall.transform.rotation);
		#endif

		newWall.transform.parent = wallRoot.transform;
	}	

	private void SingleFood(float x, float y, float z){
		GameObject food = Random.Range (0, 1f) < coolFoodPercent ? foodCool : this.food;
		GameObject newFood;
		#if UNITY_EDITOR 
		newFood = (GameObject)UnityEditor.PrefabUtility.InstantiatePrefab (food);
		newFood.transform.position = new Vector3(x, y, z);
		#else

		newFood = (GameObject)Instantiate(food, new Vector3(x, y, z), food.transform.rotation);
		#endif

		newFood.transform.parent = foodRoot.transform;
		field.foodCount++;
	}

	private void SingleTrap(float x, float y, float z){
		GameObject newTrap;
		#if UNITY_EDITOR 
		newTrap = (GameObject)UnityEditor.PrefabUtility.InstantiatePrefab (trap);
		newTrap.transform.position = new Vector3(x, y, z);
		#else

		newTrap = (GameObject)Instantiate(trap, new Vector3(x, y, z), trap.transform.rotation);
		#endif

		newTrap.transform.parent = trapRoot.transform;
	}
	
	public void GenRoundWall(){
		for(int x = 0; x < width; x++){
			SingleWall(x, 0f, 0f);
			SingleWall(x, 0f, depth-1);
		}
		
		for(int z = 1; z < depth-1; z++){
			SingleWall(0f, 0f, z);
			SingleWall(width-1f, 0f, z);
		}
	}
}
