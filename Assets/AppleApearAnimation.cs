﻿using UnityEngine;
using System.Collections;

public class AppleApearAnimation : MonoBehaviour {

	// Use this for initialization
	void Start () {
		StartCoroutine (Anim());
	}
	public float animationLength = 0.4f;
	public float finalSize = 0.7f;
	IEnumerator Anim(){
		float t = 0f;
		while (t < animationLength) {
			t += Mathf.Min(Time.deltaTime, animationLength - t);
			float size = Mathf.Sin(t / animationLength * Mathf.PI / 2f) * finalSize;
			transform.localScale = new Vector3 (size, size, size);
			yield return null;
		}
	}
}
