﻿using UnityEngine;
using System.Collections;

public class SnakeIconHighlight : MonoBehaviour {
	public Color selectedColor = Color.white;
	public Color unselectedColor = Color.gray;
	public int thisskinid;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
		if (PlayerPrefs.GetInt ("Skin") == thisskinid) {
			GetComponent<UnityEngine.UI.Image> ().color = selectedColor;
		}else
			GetComponent<UnityEngine.UI.Image> ().color = unselectedColor;

	}
}
