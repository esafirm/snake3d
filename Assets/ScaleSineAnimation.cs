﻿using UnityEngine;
using System.Collections;

public class ScaleSineAnimation : MonoBehaviour {
	public float freq = 0f;
	public float amplitude = 1f;
	Vector3 initScale;
	// Use this for initialization
	void Start () {
		initScale = transform.localScale;
	}
	
	// Update is called once per frame
	void Update () {
		float s = amplitude * Mathf.Sin(Time.time * freq * Mathf.PI * 2f); 
		transform.localScale = new Vector3 (s, s, s) + initScale;
	}
}
