﻿using UnityEngine;
using System.Collections;

public class SnakeSummary : MonoBehaviour {
	public enum Skin{
		Yellow, Rainbow, Tron
	}

	public Skin skin = Skin.Yellow;
	public Material yellowMat;
	public Material yellowMatHead;
	public Material rainbowMat;
	public Material rainbowMatHead;

	//public bool doit;
	// Use this for initialization
	void Start () {
			
		if (PlayerPrefs.HasKey ("Skin")) {
			skin = (Skin)(PlayerPrefs.GetInt ("Skin"));
		} else {
			skin = Skin.Yellow;
			PlayerPrefs.SetInt ("Skin", (int)skin);
			}

		if (skin == Skin.Yellow) {
			transform.FindChild ("SnakeMesh").GetComponent<MeshRenderer> ().materials[0] = yellowMat;
			transform.FindChild ("SnakeMesh").GetComponent<MeshRenderer> ().materials[1] = yellowMatHead;
		}
		if (skin == Skin.Rainbow) {
			var materials = new Material[2];
			materials [0] = rainbowMat;
			materials [1] = rainbowMatHead;
			transform.FindChild ("SnakeMesh").GetComponent<MeshRenderer> ().materials = materials;
			transform.FindChild ("SnakeMesh").GetComponent<SnakeProcedural> ().radius = 0.12f;
			transform.FindChild ("SnakeMesh").GetComponent<SnakeProcedural> ().middleIndent.y = 0.3f;
			//transform.FindChild ("SnakeMesh").GetComponent<MeshRenderer> ().sharedMaterials[0] = null;
			//transform.FindChild ("SnakeMesh").GetComponent<MeshRenderer> ().sharedMaterials[1] = null;
		}
		if (skin == Skin.Tron) {
			transform.FindChild ("SnakeMesh").GetComponent<MeshRenderer> ().enabled = false;
		}

	}
	
	// Update is called once per frame
	void Update () {

		if (skin == Skin.Tron) {
			foreach (Renderer r in transform.FindChild("SnakeImaginary").GetComponentsInChildren<Renderer>()) {
				r.enabled = true;
			}
		}
	}
}
